import React, { useState } from 'react';
import { Box } from '@mui/material';
import Sidebar from '../components/Common/Sidebar';


const Layout = ({ children }) => {
  const [sidebarOpen, setSidebarOpen] = useState(true);

  const handleDrawerOpen = () => {
    setSidebarOpen(true);
  };

  const handleDrawerClose = () => {
    setSidebarOpen(false);
  };
  return (
    <Box
      sx={{
        width: '100%',
        minHeight: '100vh',
        backgroundColor: 'background.default',
        display: 'flex'
      }}
    >
      <Sidebar open={sidebarOpen} toggleDrawer={sidebarOpen ? handleDrawerClose : handleDrawerOpen} />
      { children }
    </Box>
  );
};

export default Layout;