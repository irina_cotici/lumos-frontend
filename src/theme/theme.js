import { createTheme } from '@mui/material/styles';

const theme = createTheme({
  palette: {
    primary: {
      main: '#D48166',
      hover: '#BD7159',
      opaque: 'rgba(212, 129, 102, 0.7)'
    },
    secondary: {
      main: '#E6E2DD',
    },
    tertiary: {
      main: '#6A5B52',
    },
    quaternary: {
      main: '#000000',
      opaque: '#999999'
    },
    background: {
      default: '#E6E2DD',
      secondary: '#FFFFFF',
      tertiary: '#F8EFE2'
    }
  },
  shape: {
    borderRadius: 4,
  },
  typography: {
    fontFamily: 'Inter, system-ui, Avenir, Helvetica, Arial, sans-serif',
    fontSize: 18,
    h1: {
      fontSize: '28px',
      fontWeight: '700'
    },
    h2: {
      fontSize: '1.5rem',
      fontWeight: '700'
    },
    h5: {
      fontSize: '24px',
      fontWeight: '700'
    },
  },
  breakpoints: {
    values: {
      xs: 320,
      sm: 768,
      md: 960,
      lg: 1280,
      xl: 1920,
    },
  },
  transitions: {
    duration: {
      short: 200,
      medium: 400,
      long: 800,
    },
  },
  components: {
    MuiButton: {
      styleOverrides: {
        containedPrimary: {
          color: 'white',
        },
        root: {
          borderRadius: '20px',
          height: '50px'
        }
      },
    },
    MuiOutlinedInput: {
      styleOverrides: {
        root: {
          borderRadius: '20px',
          '& .MuiInputBase-input::placeholder': {
            fontSize: '0.875rem',
          },
        },
      },
    },
    MuiTextField: {
      styleOverrides: {
        root: {
          '& .MuiOutlinedInput-root': {
            borderRadius: '20px',
          },
          '& .MuiInputBase-input::placeholder': {
            fontSize: '0.875rem',
          },
          '& .MuiOutlinedInput-root textarea': {
            padding: '5px',
            fontSize: '16px'
          },
        },
      },
    },
    MuiFormControlLabel: {
      styleOverrides: {
        label: {
          fontSize: '16px',
        },

      },
    },
    MuiCheckbox: {
      styleOverrides: {
        root: {
          padding: '4px',
        },
      },
    },
    MuiInputBase: {
      styleOverrides: {
        input: {
          borderRadius: '20px',
          '&::placeholder': { 
            fontSize: '1rem',
          },
        },
      },
    },
    MuiInputLabel: {
      styleOverrides: {
        root: {
          fontSize: '1rem',
        },
      },
    },
    MuiIconButton: {
      styleOverrides: {
        root: {
          '&:focus': {
            outline: 'none',
            border: 'none',
          },
          borderRadius: 0
        },
      },
    },
    MuiDateCalendar: {
      styleOverrides: {
        dayWithMargin: {
          margin: 2,
        },
        today: {
          color: 'red',
        },
        daySelected: {
          backgroundColor: '#D48166',
          color: 'white',
          '&:hover': {
            backgroundColor: '#b34736',
          }
        },
        MuiPickersDay: {
          color: 'white',
        }
      }
    },
    MuiCssBaseline: {
      styleOverrides: {
        '::-webkit-scrollbar': {
          width: '12px',
          marginRight: '10px'
        },
        '::-webkit-scrollbar-track': {
          background: '#ffffff',
          borderRadius: '10px'
        },
        '::-webkit-scrollbar-thumb': {
          background: '#888',
          borderRadius: '8px',
          border: '4px solid #ffffff',
          backgroundColor: '#c3c3c3',
          '&:hover': {
            background: '#555',
          },
        },
      },
    },
  },
});

export default theme;