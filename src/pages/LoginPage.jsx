import React from 'react';
import { Container, Typography, TextField, Button, Box } from '@mui/material';
import Logo from '../assets/images/logo1.svg'

const LoginPage = () => {
  return (
    <Container component="main" sx={{ backgroundColor: 'background.secondary', padding: '50px !important', borderRadius: '48px', width: '460px' }}>
      <Box
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <img src={Logo} alt="Logo" style={{ width: '100px', marginBottom: '50px' }} />
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <Box component="form" sx={{ mt: 1, borderRadius: '20px' }}>
          <TextField
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Sign In
          </Button>
        </Box>
      </Box>
    </Container>
  );
}

export default LoginPage;
