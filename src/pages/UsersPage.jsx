import React from 'react';
import Container from '../components/Common/Container'
import Header from '../components/Common/Header';
import UsersList from '../components/User/UsersList';


const UsersPage = () => {

    return (
        <>
            <Container style={{ p: 3 }}>
                <Header />
                <UsersList />
                
            </Container>
        </>
    )
}

export default UsersPage;