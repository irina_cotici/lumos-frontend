import React, { useEffect, useState } from 'react';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import Container from '../../components/Common/Container';
import Header from '../../components/Common/Header';
import { fetchCourseDetail } from '../../slices/courseSlice';
import CourseHeader from '../../components/Courses/CourseHeader';
import TeacherInfo from '../../components/Courses/TeacherInfo';
import CourseDescription from '../../components/Courses/CourseDescription';
import ModuleList from '../../components/Courses/ModuleList';
import TestList from '../../components/Courses/TestList';
import { Grid, Divider, Box, Typography } from '@mui/material';
import Settings from '@mui/icons-material/MoreHorizRounded';
import TestDetail from '../../components/Tests/TestDetail';

const CourseDetails = () => {
    const navigate = useNavigate();
    const [searchParams, setSearchParams] = useSearchParams();
    const { courseId } = useParams();
    const dispatch = useDispatch();
    const course = useSelector(state => state.courses.courseDetail);
    const [activeContent, setActiveContent] = useState({ type: 'course', data: null });
    const fetchedTest = useSelector(state => state.test.test);

     // useEffect(() => {
    //     dispatch(fetchCourseDetail(courseId));
    // }, [dispatch, courseId]);
    // useEffect(() => {
    //     dispatch(fetchCourseDetail(courseId));
    //     // Fetch based on ID if needed
    //     const moduleId = searchParams.get('module');
    //     const testId = searchParams.get('test');
    //     if (moduleId) {
    //         // Dispatch fetch module detail action
    //     }
    //     if (testId) {
    //         // Dispatch fetch test detail action
    //     }
    // }, [dispatch, courseId, searchParams]);
    

    const handleModuleClick = module => {
        setActiveContent({ type: 'module', data: module });
        setSearchParams({ module: module.id });
    };

    const handleTestClick = test => {
        console.log(fetchedTest)
        setActiveContent({ type: 'test', data: test });
        setSearchParams({ test: test.id });
    };

    const handleTitleClick = () => {
        console.log('apa')
        setActiveContent({ type: 'course', data: null });
        setSearchParams({});
    };

    const renderContent = () => {
        if (activeContent.type === 'course') {
            return (
                <Box sx={{ display: 'flex'}}>
                    <TeacherInfo teacher={course.teacher} schedule={course.schedule} />
                    <CourseDescription description={course.description} />
                </Box>
            );
        } else if (activeContent.type === 'test') {
            // const { name, description, duration } = activeContent.data;
            // return (
            //     <Box>
            //         <Typography variant="h4">{name}</Typography>
            //         <Typography variant="body1">{description}</Typography>
            //         <Typography variant="body2">{`Duration: ${duration}`}</Typography>
            //     </Box>
            // );
            return <TestDetail test={fetchedTest}/>
        } else {
            return (
                <Box>
                    <Typography variant="h4">{name}</Typography>
                    <Typography variant="body1">{description}</Typography>
                    <Typography variant="body2">{`Duration: ${duration}`}</Typography>
                </Box>
            );
        }
    };

    return (
        <Container style={{ p: 3, position: 'relative' }}>
            <Header />
            <Grid container spacing={2} sx={{ paddingY: 3, paddingX: 8, overflow: 'auto', height: 'calc(100% - 130px)' }}>
                <Grid item xs={12} md={8} sx={{ paddingX: 8 }}>
                    <CourseHeader title={course.title} imageUrl={course.imageUrl} onTitleClick={handleTitleClick} activeContent={activeContent.type}/>
                    {renderContent()}
                    <Divider sx={{ my: 2 }} />
                </Grid>
                <Grid item xs={12} md={4}>
                    <Typography sx={{ marginBottom: 2 }}>Modules</Typography>
                    <ModuleList
                        modules={course.modules}
                        onModuleClick={handleModuleClick}
                        activeId={activeContent.type === 'module' ? activeContent?.data?.id : null}
                    />
                    <Divider sx={{ my: 2 }} />
                    <Typography sx={{ marginBottom: 2 }}>Tests</Typography>
                    <TestList
                        tests={course.tests}
                        onTestClick={handleTestClick}
                        activeId={activeContent.type === 'test' ? activeContent?.data?.id : null}
                    />
                </Grid>
            </Grid>        
        </Container>
    );
};

export default CourseDetails;
