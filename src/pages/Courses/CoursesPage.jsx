import React from 'react';
import { useSelector } from 'react-redux';
import Container from '../../components/Common/Container'
import Header from '../../components/Common/Header';
import CoursesList from '../../components/Courses/CourseList';


const CoursesPage = () => {
    const courses = useSelector(state => state.courses.courses);

    return (
        <>
            <Container style={{ p: 3, position: 'relative' }}>
                <Header />
                <CoursesList />
                
            </Container>
        </>
    )
}

export default CoursesPage;