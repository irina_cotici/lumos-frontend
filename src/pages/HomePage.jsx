import React from 'react';
import { useSelector } from 'react-redux';
import Container from '../components/Common/Container'
import Header from '../components/Common/Header';
import Calendar from '../components/Common/Calendar'
import PointsCharts from '../components/User/PointsCharts';
import CourseSwiper from '../components/Common/Swiper';
import { Typography } from '@mui/material';


const HomePage = () => {
    const courses = useSelector(state => state.courses.courses);
    return (
        <>
            <Container style={{ p: 3, overflow: 'auto hidden' }}>
                <Header />
                <div style={{ display: 'grid', gridTemplateColumns: '65% 30%', justifyContent: 'space-between', gap: '50px', padding: '20px 60px' }}>
                    <PointsCharts />
                    <Calendar />
                </div>
				<Typography variant="h5" component="h5" sx={{ padding: '30px 60px' }}>Courses</Typography>
                <CourseSwiper
                    items={courses}
                    slidesPerView={4.4}
                    spaceBetween={30}
                />
            </Container>
        </>
    )
}

export default HomePage;