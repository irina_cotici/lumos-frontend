import React from 'react';
import Container from '../components/Common/Container'

const LeaderBoard = () => {
    return (
        <>
            <Container style={{ p: 3 }}>
                Leader board
            </Container>
        </>
    )
}

export default LeaderBoard;