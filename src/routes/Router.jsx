import React from 'react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';

import Users from '../pages/UsersPage';
import LoginPage from '../pages/LoginPage';
import RegisterPage from '../pages/RegisterPage';
import Home from '../pages/HomePage';
import LeaderBoard from '../pages/LeaderBoardPage';
import Courses from '../pages/Courses/CoursesPage';
import CourseDetails from '../pages/Courses/CourseDetails';

import Layout from '../layouts/DefaultLayout';

const AppRouter = () => {
  return (
    <BrowserRouter>
      <Layout>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/users" element={<Users />} />
          <Route path="/courses" element={<Courses />} />
          <Route path="/courses/:id" element={<CourseDetails />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/register" element={<RegisterPage />} />
          <Route path="/leader-board" element={<LeaderBoard />} />
        </Routes>
      </Layout>
    </BrowserRouter>
  );
};

export default AppRouter;
