import React, { useState } from 'react';
import Modal from 'react-modal';

const ScoreModal = ({ isOpen, onRequestClose, onSaveScore }) => {
  const [score, setScore] = useState('');

  const handleSaveScore = () => {
    onSaveScore(score);
    setScore('');
    onRequestClose();
  };

  return (
    <Modal
      isOpen={isOpen}
      onRequestClose={onRequestClose}
      contentLabel="Score Modal"
    >
      <h2>Enter Score</h2>
      <input
        type="number"
        placeholder="Score"
        value={score}
        onChange={(e) => setScore(e.target.value)}
      />
      <button onClick={handleSaveScore}>Save</button>
    </Modal>
  );
};

export default ScoreModal;