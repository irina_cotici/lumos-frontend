import React from 'react';
import { List, ListItem, ListItemIcon, ListItemText, Typography, Badge, IconButton } from '@mui/material';
import StarBorderIcon from '@mui/icons-material/StarBorder';
import ModuleIcon from '@mui/icons-material/FormatListBulletedRounded';
import theme from '../../theme/theme';

const ModuleList = ({ modules, onModuleClick, activeId }) => (
    <List sx={{ padding: 0 }}>
        {modules.map((module, index) => (
            <ListItem key={module.id}
                sx={{
                    display: 'flex',
                    alignItems: 'center',
                    padding: '8px 0',
                    cursor: 'pointer'
                }}
                onClick={() => onModuleClick(module)}
            >
                <ListItemIcon sx={{ minWidth: '30px', marginRight: 2 }}>
                    <IconButton
                        sx={{
                            padding: '15px',
                            borderRadius: '20px',
                            '&:hover': { backgroundColor: theme.palette.primary.hover },
                            backgroundColor: module.id === activeId ? theme.palette.tertiary.main : theme.palette.primary.main
                        }}
                    >
                        <ModuleIcon sx={{ color: 'white' }} />
                    </IconButton>
                </ListItemIcon>
                <ListItemText
                    primary={
                        <Typography variant="body1" sx={{ color: module.id === activeId ? theme.palette.tertiary.main : theme.palette.quaternary.main }}>
                            {module.name}
                        </Typography>
                    }
                    secondary={
                        <Typography variant="body2" color="text.secondary">
                            {`\n${module.description} - ${module.duration}`}
                        </Typography>
                    }
                />
            </ListItem>
        ))}
    </List>
);

export default ModuleList;