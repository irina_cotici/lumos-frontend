import React from 'react';
import { Typography } from '@mui/material';

const CourseDescription = ({ description }) => (
    <Typography variant="body1" sx={{ fontSize: '14px' }}>{description}</Typography>
);

export default CourseDescription;
