import React from 'react';
import { useNavigate } from 'react-router-dom';
import { Card, CardActionArea, CardMedia, CardContent, Typography, Chip } from '@mui/material';
import { styled } from '@mui/material/styles';

const CustomCard = styled(Card)(({ theme }) => ({
    position: 'relative',
    boxShadow: '0 8px 40px -12px rgba(0,0,0,0.3)',
    '&:hover': {
        boxShadow: '0 16px 70px -12.125px rgba(0,0,0,0.3)'
    },
    borderRadius: '40px',
    overflow: 'hidden'
}));

const CustomCardContent = styled(CardContent)(({ theme }) => ({
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    color: 'white',
    position: 'absolute',
    bottom: 0,
    width: '100%',
}));

function CourseCard({ course }) {

    const navigate = useNavigate();

    const handleCardClick = () => {
        navigate(`/courses/${course.id}`);
    };

    return (
        <CustomCard onClick={handleCardClick}>
            <CardActionArea>
                <CardMedia
                    component="img"
                    height="200"
                    image={course.imageUrl}
                    alt={course.title}
                />
                <CustomCardContent sx={{ width: '100%' }}>
                    {course.tag && (
                        <Chip label={course.tag} color="secondary" size="small" sx={{ position: 'absolute', top: 16, left: 16 }} />
                    )}
                    <Typography gutterBottom variant="h5" component="div" sx={{ fontSize: '18px' }}>
                        {course.title}
                    </Typography>
                    <Typography variant="body2" sx={{ fontSize: '14px' }}>
                        {course.teacher}
                    </Typography>
                </CustomCardContent>
            </CardActionArea>
        </CustomCard>
    );
}

export default CourseCard;
