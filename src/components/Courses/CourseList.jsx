import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { Grid, Pagination } from '@mui/material';
import CourseCard from './CourseCard';

function CoursesGrid() {
    const courses = useSelector(state => state.courses.courses);
    
    const [page, setPage] = useState(1);
    const itemsPerPage = 12;
    const count = Math.ceil(courses.length / itemsPerPage);

    const currentCourses = courses.slice((page - 1) * itemsPerPage, page * itemsPerPage);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    return (
        <>
            <Grid container spacing={2} sx={{ width: '95%', margin: 'auto' }}>
                {currentCourses.map(course => (
                    <Grid item xs={12} sm={6} md={3} key={course.id}>
                        <CourseCard course={course} />
                    </Grid>
                ))}
            </Grid>
            <Pagination
                count={count}
                page={page}
                onChange={handleChangePage}
                sx={{ 
                    padding: 2,
                    justifyContent: 'center',
                    display: 'flex',
                    position: 'absolute',
                    left: '0',
                    right: '0',
                    bottom: '20px',
                    margin: 'auto',
                    minWidth: '400px'
                }}
            />
        </>
    );
}

export default CoursesGrid;
