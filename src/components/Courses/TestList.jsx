import React from 'react';
import { List, ListItem, ListItemIcon, IconButton, ListItemText, Typography } from '@mui/material';
import Check from '@mui/icons-material/RuleRounded';
import ShortText from '@mui/icons-material/ShortTextRounded';
import File from '@mui/icons-material/AttachmentRounded';
import theme from '../../theme/theme';

const renderIcon = (type) => {
    switch (type) {
        case '1': 
            return <Check sx={{ color: 'white' }} />
        case '2': 
            return <ShortText sx={{ color: 'white' }} />
        case '3': 
            return <File sx={{ color: 'white' }} />
    }
}

const TestList = ({ tests, onTestClick, activeId }) => (
    <List sx={{ padding: 0 }}>
        {tests.map((test, index) => (
            <ListItem key={test.id} sx={{ padding: '8px 0', cursor: 'pointer' }} onClick={() => onTestClick(test)}>
                <ListItemIcon sx={{ minWidth: '30px', marginRight: 2 }}>
                    <IconButton
                        sx={{
                            padding: '15px',
                            borderRadius: '20px',
                            '&:hover': { backgroundColor: theme.palette.primary.hover },
                            backgroundColor: test.id === activeId ? theme.palette.tertiary.main : theme.palette.primary.main
                        }}
                    >
                        {renderIcon(test.type)}
                    </IconButton>
                </ListItemIcon>
                <ListItemText 
                    primary={
                        <Typography
                            variant="body1"
                            sx={{ color: test.id === activeId ? theme.palette.tertiary.main : theme.palette.quaternary.main }}
                        >
                            {test.name}
                        </Typography>
                    } 
                    secondary={<Typography variant="body2" color="text.secondary">{`Lesson ${index + 4} - ${test.duration}`}</Typography>}
                />
            </ListItem>
        ))}
    </List>
);

export default TestList;
