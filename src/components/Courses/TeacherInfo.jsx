import React from 'react';
import { Box, Typography, CardMedia } from '@mui/material';
import theme from '../../theme/theme';

const TeacherInfo = ({ teacher, schedule }) => (
    <Box style={{ display: 'flex' }}>
        <CardMedia
            component="img"
            height="56"
            image={teacher.imageUrl}
            alt="Teacher avatar"
            sx={{
                width: '56px',
                marginRight: 2,
                borderRadius: '50%',
                border: '1px solid',
                borderColor: theme.palette.primary.main,
                filter: 'brightness(0.9)',
                padding: '3px'
            }}
        />
        <Box sx={{ marginRight: 6 }}>
            <Typography variant="subtitle1" sx={{ width: 'max-content' }}>
                {teacher.firstName} {teacher.secondName}
            </Typography>
            <Typography variant="subtitle1"
                sx={{
                    marginBottom: 2,
                    width: '160px',
                    fontSize: '14px',
                    color: theme.palette.quaternary.opaque
                }}>
                {schedule.join(', \n')}
            </Typography>
        </Box>
    </Box>
);

export default TeacherInfo;
