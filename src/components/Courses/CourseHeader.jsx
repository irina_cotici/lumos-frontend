import React from 'react';
import { Typography, CardMedia, Box } from '@mui/material';
import Back from '@mui/icons-material/ArrowBackIosNewRounded';

const CourseHeader = ({ title, imageUrl, onTitleClick, activeContent = 'course' }) => (
    <>  
        <Box sx={{ display: 'flex', cursor: 'pointer' }} onClick={() => onTitleClick()}>
            <Back sx={{ display: activeContent !== 'course' ? 'block' : 'none' }}/>
            <Typography variant="h1" component="h1" sx={{ paddingX: 3, marginBottom: 2 }}>
                Course - {title}
            </Typography>
        </Box>
        <CardMedia
            component="img"
            height="350"
            image={imageUrl}
            alt="Course Image"
            sx={{ width: '100%', marginBottom: 5, borderRadius: '40px', filter: 'brightness(0.7)', display: activeContent === 'course' ? 'block' : 'none' }}
        />
    </>
);

export default CourseHeader;
