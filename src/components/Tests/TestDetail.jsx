import React from 'react';
import { Box, Typography, Checkbox, FormControlLabel, TextField, Button, CardMedia } from '@mui/material';
import Dropzone from '../Common/Dropzone';

const TestDetail = ({ test }) => {
    const getQuestionsWithOptions = () => {
        return test.question.map(questionText => {
            const [question, optionsString] = questionText.split(':');
            const options = optionsString?.split(', ').map(option => option.trim());
            return { question, options };
        });
    };

    const renderInputField = (options, type) => {
        switch(type) {
            case '1': // Type 1 for checkboxes
                return (
                    <Box sx={{ display: 'flex', flexDirection: 'column' }}>
                        {options.map((option, index) => (
                            <FormControlLabel
                                key={index}
                                control={<Checkbox />}
                                label={option}
                                sx={{ ml: 3 }}
                            />
                        ))}
                    </Box>
                );
            case '2': // Type 2 for textarea
                return (
                    <TextField
                        multiline
                        fullWidth
                        variant="outlined"
                        placeholder="  Type your answer here..."
                        minRows={3}
                        InputProps={{
                            style: {
                                padding: '10px 20px'
                            }
                        }}
                    />
                );
            case '3': // Type 3 for file upload
                return (
                    <Dropzone />
                );
        }
    };

    const questionsWithOptions = getQuestionsWithOptions();

    return (
        <Box>
            <Typography variant="h2" sx={{ padding: '10px 56px 20px' }}>{test.title}</Typography>
            <CardMedia
                component="img"
                height="350"
                image={test.imageUrl}
                alt="Course Image"
                sx={{ width: '100%', marginBottom: 5, borderRadius: '40px' }}
            />
            <Typography variant="body1" sx={{ margin: '20px 0', fontSize: '14px' }}>{test.description}</Typography>
            {questionsWithOptions.map(({ question, options }, index) => (
                <Box key={index} mt={2}>
                    <Typography sx={{ p: 2 }}>{question}</Typography>
                    {renderInputField(options, test.type)}
                </Box>
            ))}
            <Button
                type="submit"
                variant="contained"
                sx={{ mt: 3, mb: 2, padding: '0 50px'}}
            >
                Submit
            </Button>
        </Box>
    );
};

export default TestDetail;
