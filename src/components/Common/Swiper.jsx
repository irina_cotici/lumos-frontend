import React from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper-bundle.css';
import { Navigation } from 'swiper/modules';
import styled from '@emotion/styled';

const StyledDiv = styled('div')({
    width: '56px',
    height: '56px',
    backgroundColor: '#F6F6F5',
    borderRadius: '20px',
    top: '40%',
    '&::after': {
        content: '"next"',
        position: 'absolute',
        fontSize: '24px',
        color: '#000000',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -40%)'
    }
});

function GeneralSwiper({ items, slidesPerView, spaceBetween }) {
    return (
        <div style={{ position: 'relative' }}>
            <Swiper
                spaceBetween={spaceBetween || 50}
                slidesPerView={slidesPerView || 3}
                onSlideChange={() => console.log('slide change')}
                onSwiper={(swiper) => console.log(swiper)}
                style={{ width: '88%', marginLeft: '60px', height: '200px' }}
                modules={ [Navigation] }
                loop={ true }
                navigation={{
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                }}
            >
                {items.map(item => (
                    <SwiperSlide key={item.id} style={{ borderRadius: '40px', position: 'relative' }}>
                        <img src={item.imageUrl} alt={item.title} style={{ width: '100%', maxHeight: '100%', borderRadius: '40px', filter: 'brightness(0.7)' }} />
                        <p style={{ 
                            position: 'absolute', 
                            bottom: '20px',
                            left: '0',
                            right: '0',
                            margin: 'auto',
                            backgroundColor: 'rgb(159 112 97 / 70%)', 
                            textAlign: 'center', 
                            padding: '5px 70px',
                            maxWidth: '84px',
                            color: 'white',
                            borderRadius: '40px',
                            fontSize: '16px'
                        }}>
                            { item.title }
                        </p>
                    </SwiperSlide>
                ))}
            </Swiper>
            <StyledDiv className="swiper-button-next" />
        </div>
    );
}

export default GeneralSwiper;