import React, { useEffect } from 'react';
import { NavLink, useLocation } from 'react-router-dom';

import { styled, useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import MuiDrawer from '@mui/material/Drawer';
import MuiAppBar from '@mui/material/AppBar';
import List from '@mui/material/List';
import IconButton from '@mui/material/IconButton';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';

// icons
import LeaderBoard from '@mui/icons-material/LeaderboardRounded';
import LibraryBooksRoundedIcon from '@mui/icons-material/LibraryBooksRounded';
import BackupTableRoundedIcon from '@mui/icons-material/BackupTableRounded';
import SettingsSuggestRoundedIcon from '@mui/icons-material/SettingsSuggestRounded';
import ExtensionRoundedIcon from '@mui/icons-material/ExtensionRounded';
import ForumRoundedIcon from '@mui/icons-material/ForumRounded';
import DashboardRoundedIcon from '@mui/icons-material/DashboardRounded';

import ShortLogo from '../../assets/images/logo1.svg'
import FullLogo from '../../assets/images/logo.png'

const drawerWidth = 300;

const openedMixin = (theme) => ({
  width: drawerWidth,
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: 'hidden',
});

const closedMixin = (theme) => ({
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: 'hidden',
  width: `calc(${theme.spacing(10)} + 1px)`,
  [theme.breakpoints.up('sm')]: {
    width: `calc(${theme.spacing(15)} + 1px)`,
  },
});

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-end',
  padding: theme.spacing(0, 1),
  ...theme.mixins.toolbar,
}));

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(['width', 'margin'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
  ({ theme, open }) => ({
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
    boxSizing: 'border-box',
    ...(open && {
      ...openedMixin(theme),
      '& .MuiDrawer-paper': {
        ...openedMixin(theme),
      }
    }),
    ...(!open && {
      ...closedMixin(theme),
      '& .MuiDrawer-paper': closedMixin(theme),
    }),
    '& .MuiDrawer-paper': {
      backgroundColor: theme.palette.background.default,
      border: 0
    }
  }),
);

export default function MiniDrawer() {
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const drawerItems = [
    { text: 'Home', icon: <DashboardRoundedIcon />, link: '/' },
    { text: 'Leader Board', icon: <LeaderBoard />, link: '/leader-board' },
    { text: 'My Courses', icon: <LibraryBooksRoundedIcon />, link: '/courses' },
    { text: 'Time Table', icon: <BackupTableRoundedIcon />, link: '/timetable' },
    { text: 'Settings', icon: <SettingsSuggestRoundedIcon />, link: '/settings' },
    { text: 'Gamification settings', icon: <ExtensionRoundedIcon />, link: '/gamification-settings' },
    { text: 'Messages', icon: <ForumRoundedIcon />, link: '/chat' },
  ];

  const location = useLocation();

  return (
    <Drawer variant="permanent" open={open}>
      <DrawerHeader
        sx={{
          justifyContent: 'center',
          pt: 5,
          pb: 10,
          px: 0,
        }}
      >
        <IconButton
          onClick={handleDrawerClose}
          sx={{
            outline: 0,
            ...(!open && { display: 'none' }),
          }}
        >
          <img src={FullLogo} alt="Logo" style={{ height: '50px', width: 'auto' }} />
        </IconButton>
        <IconButton
          color="inherit"
          aria-label="open drawer"
          onClick={handleDrawerOpen}
          sx={{
            justifyContent: 'center',
            outline: 0,
            width: '100%',
            ...(open && { display: 'none' }),
          }}
        >
          <img src={ShortLogo} alt="Logo" style={{ width: '50px' }} />
        </IconButton>
      </DrawerHeader>
      <List>
      {drawerItems.map((item, index) => (
          <ListItem key={item.text} disablePadding sx={{ display: 'block', my: 1 }}>
            <ListItemButton
              component={NavLink}
              to={item.link}
              sx={{
                minHeight: 48,
                justifyContent: open ? 'initial' : 'center',
                px: 5,
                py: 3,
                overflow: 'hidden',
                transition: '0.5s',
                '&:hover': {
                  color: theme.palette.tertiary.main,
                },
                '&.active::after': {
                  content: '""',
                  position: 'absolute',
                  top: 0,
                  right: 0,
                  bottom: 0,
                  left: 20,
                  borderRadius: '100px 0 0 100px',
                  backgroundColor: theme.palette.background.secondary,
                  zIndex: 0,
                  transition: '0.5s',
                  ...({ opacity: open ? 1 : 0 }),
                },
                '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
                  position: 'relative',
                  zIndex: 1,
                },
              }}
            >
              <div style={{
                position: 'absolute',
                top: '-27px',
                bottom: 0,
                right: '-3%',
                width: '95%',
                height: 40,
                backgroundColor: theme.palette.background.default,
                borderRadius: '105px 0 105px',
                zIndex: 1,
                ...(!open && { display: 'none' }),
              }}/>
              <ListItemIcon
                sx={{
                  minWidth: 0,
                  mr: open ? 3 : 0,
                  justifyContent: 'center',
                  color: location.pathname.includes(item.link) ? theme.palette.primary.main : 'inherit',
                }}
              >
                {item.icon}
              </ListItemIcon>
              <div style={{
                position: 'absolute',
                bottom: '-27px',
                right: '-4%',
                width: '95%',
                height: 40,
                backgroundColor: theme.palette.background.default,
                borderRadius: '0 105px 0',
                zIndex: 1,
                ...(!open && { display: 'none' }),
              }}/>
              <ListItemText primary={item.text} sx={{ ...(!open && { display: 'none' }) }} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </Drawer>
  );
}
