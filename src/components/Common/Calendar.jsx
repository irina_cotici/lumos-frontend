import * as React from 'react';
import { useState } from 'react';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { StaticDatePicker } from '@mui/x-date-pickers/StaticDatePicker';
import TextField from '@mui/material/TextField';
import dayjs from 'dayjs';
import Box from '@mui/material/Box';

function Calendar() {
  const [value, setValue] = useState(dayjs());

  return (
    <Box sx={{ padding: '20px', borderRadius: '20px', boxShadow: '0px 4px 10px rgba(0, 0, 0, 0.1)', maxWidth: '320px', width: '100%', height: '330px' }}>
        <LocalizationProvider dateAdapter={ AdapterDayjs }>
        <StaticDatePicker
            displayStaticWrapperAs="desktop"
            openTo="day"
            value={ value }
            onChange={ (newValue) => {
            setValue(newValue);
            }}
            renderInput={(params) => <TextField {...params} />}
        />
        </LocalizationProvider>
    </Box>
  );
}

export default Calendar;
