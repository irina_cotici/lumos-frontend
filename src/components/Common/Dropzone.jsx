import React, { useState } from 'react';
import { useDropzone } from 'react-dropzone';
import { Box, Typography, Paper, List, ListItem, ListItemIcon, ListItemText } from '@mui/material';
import InsertDriveFileIcon from '@mui/icons-material/InsertDriveFile';

const StyledDropzone = () => {
  const [files, setFiles] = useState([]);

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop: acceptedFiles => {
      setFiles(acceptedFiles.map(file => Object.assign(file, {
        preview: URL.createObjectURL(file)
      })));
    }
  });

  return (
    <Box sx={{ p: 2 }}>
      <Box {...getRootProps()} sx={{ outline: 'none' }}>
        <Paper
          elevation={3}
          sx={{
            p: 2,
            bgcolor: 'background.default',
            textAlign: 'center',
            cursor: 'pointer',
            color: isDragActive ? 'primary.main' : 'text.secondary',
            border: isDragActive ? '2px dashed #4A90E2' : '2px dashed grey',
            '&:hover': {
              bgcolor: 'background.paper',
              opacity: [0.9, 0.8, 0.7],
            },
          }}
        >
          <input {...getInputProps()} />
          <Typography variant="body2">
            {isDragActive ? "Drop the files here ..." : "Drag 'n' drop some files here, or click to select files"}
          </Typography>
        </Paper>
      </Box>
      <List>
        {files.map(file => (
          <ListItem key={file.path}>
            <ListItemIcon sx={{ minWidth: '40px'}}>
              <InsertDriveFileIcon/>
            </ListItemIcon>
            <ListItemText 
                primary={
                    <Typography sx={{ fontSize: "14px" }}>{file.name}</Typography>
                } 
                secondary={
                    <Typography sx={{ fontSize: "12px" }}>{`Size: ${file.size} bytes`}</Typography>
                } />
          </ListItem>
        ))}
      </List>
    </Box>
  );
};

export default StyledDropzone;
