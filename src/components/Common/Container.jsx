import React from 'react';
import { Paper } from '@mui/material';

const WhiteContainer = ({ children, ...props }) => {
  return (
    <Paper
      elevation={3}
      sx={{
        borderRadius: 10,
        m: '26px',
        bgcolor: 'background.paper',
        boxShadow: '0px 4px 10px rgba(0, 0, 0, 0.1)',
        height: '90vh',
        width: 'fill-available'
      }}
      {...props}
    >
      {children}
    </Paper>
  );
};

export default WhiteContainer;
