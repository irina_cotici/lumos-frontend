import React from 'react';
import { AppBar, Toolbar, IconButton, InputBase, Badge, Box } from '@mui/material';
import { Search as SearchIcon, AccountCircle } from '@mui/icons-material';
import Notifications from '@mui/icons-material/NotificationsActiveOutlined';
import { ThemeProvider } from '@mui/material/styles';
import theme from '../../theme/theme';

export default function PrimarySearchAppBar() {
  return (
    <ThemeProvider theme={theme}>
      <AppBar position="static" sx={{ backgroundColor: 'transparent', boxShadow: 'none', padding: '0px 30px' }}>
        <Toolbar sx={{ display: 'flex', justifyContent: 'space-between' }}>
          <Box style={{ position: 'relative', maxWidth: '70%', borderRadius: '20px', backgroundColor: theme.palette.background.default, marginRight: 2, marginLeft: 0, width: '100%', padding: '11px' }}>
            <Box style={{ padding: '0 16px', height: '100%', position: 'absolute', top: 0, pointerEvents: 'none', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
              <SearchIcon/>
            </Box>
            <InputBase
              placeholder="Search…"
              sx={{ color: 'inherit', paddingLeft: 'calc(1em + 32px)', width: '100%' }}
            />
          </Box>
          <Box>
            <IconButton
              edge="end"
              color="inherit"
              sx={{ margin: '20px', padding: '15px' }}
            >
              <AccountCircle />
            </IconButton>
            <IconButton sx={{ padding: '15px', backgroundColor: theme.palette.primary.main, borderRadius: '20px', '&:hover': { backgroundColor: theme.palette.primary.hover } }}>
              <Badge badgeContent={4} color="secondary">
                <Notifications sx={{ color: 'white' }} />
              </Badge>
            </IconButton>
          </Box>
        </Toolbar>
      </AppBar>
    </ThemeProvider>
  );
}