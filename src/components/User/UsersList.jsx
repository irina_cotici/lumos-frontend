import React from 'react';
import { useSelector } from 'react-redux';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import Avatar from '@mui/material/Avatar';
import IconButton from '@mui/material/IconButton';
import InfoIcon from '@mui/icons-material/MoreVertRounded';
import { styled } from '@mui/material/styles';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    fontWeight: 'bold',
    backgroundColor: theme.palette.primary.hover,
    color: 'white',
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
        backgroundColor: theme.palette.action.hover,
    },
    '&:last-child td, &:last-child th': {
        border: 0,
    },
}));

const CustomTableContainer = styled(TableContainer)(({ theme }) => ({
    borderRadius: '16px',
    overflow: 'hidden',
    padding: '0',
    background: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
}));

function UsersList() {
    const courses = useSelector(state => state.courses.courses);

    return (
        <CustomTableContainer component={Paper} sx={{ marginX: '50px', marginY: '50px', width: "94%" }}>
            <Table aria-label="simple table">
                <TableHead>
                    <StyledTableRow>
                        <StyledTableCell>Course</StyledTableCell>
                        <StyledTableCell align="right">Teacher</StyledTableCell>
                        <StyledTableCell align="center">Schedule</StyledTableCell>
                        <StyledTableCell align="right">Progress (%)</StyledTableCell>
                        <StyledTableCell align="right">&nbsp;</StyledTableCell>
                    </StyledTableRow>
                </TableHead>
                <TableBody>
                    {courses.map((course) => (
                        <StyledTableRow key={course.id}>
                            <TableCell component="th" scope="row" sx={{ display: 'flex', alignItems: 'center', height: '47px' }}>
                                <Avatar alt={course.title} src={course.imageUrl} sx={{ mr: 5 }} />
                                {course.title}
                            </TableCell>
                            <TableCell align="right">{course.teacher}</TableCell>
                            <TableCell align="center">
                                {course.schedule.join(', ')}
                            </TableCell>
                            <TableCell align="right">{course.progress}</TableCell>
                            <TableCell align="right">
                                <IconButton color="primary" aria-label="view details">
                                    <InfoIcon />
                                </IconButton>
                            </TableCell>
                        </StyledTableRow>
                    ))}
                </TableBody>
            </Table>
        </CustomTableContainer>
    );
}

export default UsersList;
