// Import React and Highcharts
import React from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import Box from '@mui/material/Box';
import theme from '../../theme/theme';


const PointsCharts = () => {
    const options = {
        chart: {
            type: 'column',
            backgroundColor: theme.palette.background.default,
            height: '330px'
        },
        title: {
            text: 'Monthly Class Performance'
        },
        xAxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May']
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Points'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold'
                }
            },
            gridLineColor: '#000000',
            gridLineWidth: 1,
            gridLineDashStyle: 'Dash'
        },
        tooltip: {
            valueSuffix: ' points'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true
                },
                borderRadius: 15,
                borderWidth: 0,
            },
        },
        series: [{
            name: 'My Points',
            data: [50, 70, 60, 80, 90],
            color: theme.palette.primary.main
        }, {
            name: 'Class Points',
            data: [30, 60, 40, 70, 50],
            color: theme.palette.background.tertiary
        }],
        credits: {
            enabled: false
        }
    };

    return (
        <Box sx={{ borderRadius: '40px', boxShadow: '0px 4px 10px rgba(0, 0, 0, 0.1)', padding: '20px', backgroundColor: '#E6E2DD', height: '370px' }}>
            <HighchartsReact
                highcharts={Highcharts}
                options={options}
            />
        </Box>
    );
};

export default PointsCharts;
