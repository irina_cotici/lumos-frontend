import './App.css'
import Router from './routes/Router';
import { ThemeProvider } from '@mui/material/styles';
import theme from './theme/theme';
import { Provider } from 'react-redux';
import store from './slices/store'
import CssBaseline from '@mui/material/CssBaseline';

function App() {

  // if (process.env.NODE_ENV === 'development') {
// }

  return (
    <>
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <main style={{ backgroundColor: '#e6e2dd' }}>
          <Router />
        </main>
      </ThemeProvider>
    </Provider>
    </>
  )
}

export default App
