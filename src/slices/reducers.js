import { combineReducers } from 'redux';
import userReducer from './userSlice';
import coursesReducer from './courseSlice';
import testReducer from './testSlice'

const rootReducer = combineReducers({
  user: userReducer,
  courses: coursesReducer,
  test: testReducer
});

export default rootReducer;
