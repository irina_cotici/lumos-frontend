// features/courses/coursesSlice.js
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

export const fetchCourseDetail = createAsyncThunk(
    'courses/fetchDetail',
    async (courseId, thunkAPI) => {
        // const response = await fetch(`https://api.example.com/courses/${courseId}`);
        // const data = await response.json();
        // return data;
    }
);

const initialState = {
    courses: [
        { id: 1, title: '11th Grade Informatics', imageUrl: 'https://images.unsplash.com/photo-1501504905252-473c47e087f8?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8Y291cnNlc3xlbnwwfHwwfHx8MA%3D%3D', teacher: "John Doe", schedule: ["Mon 10:00-12:00", "Wed 14:00-16:00"], progress: 75 },
        { id: 2, title: '11th Grade Mathematics', imageUrl: 'https://images.unsplash.com/photo-1503428593586-e225b39bddfe?q=80&w=1740&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D', teacher: "John Doe", schedule: ["Mon 10:00-12:00", "Wed 14:00-16:00"], progress: 75 },
        { id: 3, title: '11th Grade Biology', imageUrl: 'https://images.unsplash.com/photo-1517048676732-d65bc937f952?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D', teacher: "John Doe", schedule: ["Mon 10:00-12:00", "Wed 14:00-16:00"], progress: 75 },
        { id: 4, title: '11th Grade English', imageUrl: 'https://images.unsplash.com/photo-1508830524289-0adcbe822b40?q=80&w=2025&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D', teacher: "John Doe", schedule: ["Mon 10:00-12:00", "Wed 14:00-16:00"], progress: 75 },
        { id: 5, title: '11th Grade Chemistry', imageUrl: 'https://images.unsplash.com/photo-1532618500676-2e0cbf7ba8b8?q=80&w=2020&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D', teacher: "John Doe", schedule: ["Mon 10:00-12:00", "Wed 14:00-16:00"], progress: 75 },
        { id: 6, title: '11th Grade English', imageUrl: 'https://plus.unsplash.com/premium_photo-1661284886645-1e21653e252a?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D', teacher: "John Doe", schedule: ["Mon 10:00-12:00", "Wed 14:00-16:00"], progress: 75 },
        { id: 7, title: '11th Grade English', imageUrl: 'https://images.unsplash.com/photo-1526374965328-7f61d4dc18c5?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D', teacher: "John Doe", schedule: ["Mon 10:00-12:00", "Wed 14:00-16:00"], progress: 75 },
        { id: 8, title: '11th Grade English', imageUrl: 'https://images.unsplash.com/photo-1508830524289-0adcbe822b40?q=80&w=2025&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D', teacher: "John Doe", schedule: ["Mon 10:00-12:00", "Wed 14:00-16:00"], progress: 75 },
        { id: 9, title: '11th Grade Chemistry', imageUrl: 'https://images.unsplash.com/photo-1532618500676-2e0cbf7ba8b8?q=80&w=2020&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D', teacher: "John Doe", schedule: ["Mon 10:00-12:00", "Wed 14:00-16:00"], progress: 75 },
        { id: 10, title: '11th Grade English', imageUrl: 'https://plus.unsplash.com/premium_photo-1661284886645-1e21653e252a?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D', teacher: "John Doe", schedule: ["Mon 10:00-12:00", "Wed 14:00-16:00"], progress: 75 },
        { id: 11, title: '11th Grade English', imageUrl: 'https://images.unsplash.com/photo-1526374965328-7f61d4dc18c5?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D', teacher: "John Doe", schedule: ["Mon 10:00-12:00", "Wed 14:00-16:00"], progress: 75 }
        // add more courses as needed
    ],
    courseDetail: {
        id: 1,
        title: 'A Journey through Physics',
        imageUrl: 'https://images.unsplash.com/photo-1416339306562-f3d12fefd36f?ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60',
        description: 'Embark on a fascinating adventure through the depths of space and time in our "Exploring the Cosmos" course. Discover the wonders of the universe, from the birth of stars to the mysteries of black holes, while gaining hands-on experience with telescopes and astronomical data analysis. Join us as we unravel the secrets of the cosmos and explore our place in the vastness of space.',
        teacher: {
            firstName: 'Jane',
            secondName: 'Smith',
            imageUrl: 'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?q=80&w=2080&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D'
        },
        schedule: ['Monday 10:00-12:00', 'Wednesday 14:00-16:00'],
        progress: 75,
        modules: [
            {
                id: "1",
                name: "Module #1 - Lorem ipsum dolores",
                description: "Introduction to basic concepts of physics and their real-world applications.",
                duration: "2 weeks"
            },
            {
                id: "2",
                name: "Module #2 - Theories of Relativity",
                description: "Explore Einstein's theories of relativity and their implications for modern physics.",
                duration: "3 weeks"
            }
        ],
        tests: [
            {
                id: "1",
                name: "Stellar Birth and Evolution",
                duration: "90 min",
                type: "1"
            },
            {
                id: "2",
                name: "Planetary Systems",
                duration: "1 week",
                type: "2"
            },
            {
                id: "3",
                name: "Expanding Universe",
                duration: "1 week",
                type: "3",
            },
            {
                id: "4",
                name: "Black Holes and Dark Matter",
                duration: "5 hours",
                type: "1"
            },
            {
                id: "5",
                name: "Theoretical Astrophysics",
                duration: "2 hours",
                type: "3"
            }
        ]
    },
    status: 'idle', // 'idle' | 'loading' | 'succeeded' | 'failed'
    error: null
};

const coursesSlice = createSlice({
    name: 'courses',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(fetchCourseDetail.pending, (state) => {
                state.status = 'loading';
            })
            .addCase(fetchCourseDetail.fulfilled, (state, action) => {
                state.status = 'succeeded';
                state.courseDetail = action.payload;
            })
            .addCase(fetchCourseDetail.rejected, (state, action) => {
                state.status = 'failed';
                state.error = action.error.message;
            });
    }
});

export default coursesSlice.reducer;