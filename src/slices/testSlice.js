// features/courses/coursesSlice.js
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';

export const fetchTest = createAsyncThunk(
    'courses/fetchTest',
    async (courseId, thunkAPI) => {
    }
);

const initialState = {
    test: {
        id: 1,
        title: 'Test #1',
        question: ['What is the name of the curse that killed Harry\'s parents?'],
        type: '3',
        imageUrl: 'https://images.unsplash.com/photo-1518133910546-b6c2fb7d79e3?q=80&w=1935&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
        description: 'Going all the way back to his time living with the Dursleys all the way to his final showdown with the Dark Lord, the adventures of Harry Potter have provided countless children, adolescents and adults hours and hours of entertainment.',
        duration: '1h',
        deadline: '1716888327',
    },
    // test: {
    //     id: 1,
    //     title: 'Test #1',
    //     question: ['What is the name of the curse that killed Harry\'s parents? : Ron, Hermione', 'What is the name of the curse that killed Harry\'s parents? : Ron, Hermione'],
    //     type: '1',
    //     imageUrl: 'https://images.unsplash.com/photo-1518133910546-b6c2fb7d79e3?q=80&w=1935&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D',
    //     description: 'Going all the way back to his time living with the Dursleys all the way to his final showdown with the Dark Lord, the adventures of Harry Potter have provided countless children, adolescents and adults hours and hours of entertainment.',
    //     duration: '1h',
    //     deadline: '1716888327',
    // },
    status: 'idle', // 'idle' | 'loading' | 'succeeded' | 'failed'
    error: null
};

const testSlice = createSlice({
    name: 'test',
    initialState,
    reducers: {},
    extraReducers: (builder) => {
        builder
            .addCase(fetchTest.pending, (state) => {
                state.status = 'loading';
            })
            .addCase(fetchTest.fulfilled, (state, action) => {
                state.status = 'succeeded';
                state.courseDetail = action.payload;
            })
            .addCase(fetchTest.rejected, (state, action) => {
                state.status = 'failed';
                state.error = action.error.message;
            });
    }
});

export default testSlice.reducer;